package her.blok2.b_ict.hz.com.blok2her.model;

import java.util.ArrayList;

/**
 * Created by anton on 21-1-16.
 * defines a chart
 */
public class Chart {
    private static ArrayList<MusicItem> musicItems = new ArrayList<MusicItem>();

    /**
     * list all musicItems
     * @return a list of all musicItems
     */
    public static ArrayList<String> listMusicItems(){
        ArrayList<String> returnValue=new ArrayList<String>();
        for (int i=0;i< musicItems.size();i++){
            MusicItem musicItem= musicItems.get(i);
            String s=musicItem.toString();
            returnValue.add(s);
        }
        return returnValue;
    }
}
